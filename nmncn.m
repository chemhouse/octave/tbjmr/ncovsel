function z = nmncn(x)
% mean centers a ND-array along first dimension
% z = nmncn(x)
%

s = size(x);

z = reshape(x,[s(1),prod(s(2:end))]);

mz = mean(z);
mcz = z - repmat(mz, s(1), 1);

z = reshape(mcz,s);
