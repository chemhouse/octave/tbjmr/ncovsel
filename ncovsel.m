function [num,model] = ncovsel(x, y, o, lv, deflat_type);
%ncovsel       - Selection of features in a ND array from covariance maximisation
%  [num,model] = ncovsel(X,Y,orders,nf,deflation)
%
% Input arguments
%================
% X         : N-D Array N x I x J X ...
% Y         : response matrix N x Q
% orders    : orders of the features to search for (1->I, 2->J, [1 3]-> I&K, etc.)
% nf        : number of features to select
% deflation : deflation type 1: full rank, 2: rank one. Optional, default=1
%
% Output arguments
%================
% num   : rank of the selected features in each order
% model : detailed results
%
%  example :
%
%     let x be a 4-D array (100,5,4,10);
%     and y be a matrix of concentrations (100,2)
% 
% [num,model] = ncovsel(x, y, 1, 10); returns the 10 most important
%   features along the first variable mode, i.e. of size (100,4,10)
%
% [num,model] = ncovsel(x, y, [2 3], 10); returns the 10 most important
%   features along variable modes 2 and 3 i.e. of size (100,5)

% coherence test
if size(x,1) ~= size(y,1)
    error('dimensions of x and y do not match');
end;

% manage default values
if nargin < 5
    deflat_type = 1;
end;

% init outputs
num = [];

if nargout > 1
    model.num = [];
    model.crbvx = [];
    model.crbvy = [];
    model.covx = {};
    model.defx = {};
end;

% number of samples
N = size(y,1);

% to retrieve the slices...
s = size(x);
nd = length(s);

o = o + 1;  % to skip the sample mode

subs = s; subs(o)=[]; % size of a slice

o1 = 1:nd;  % reorganizing x
o1(o) = []; % put the searched orders at the end
o2 = [o1,o];
x = permute(x,o2);
sx = size(x);   % size of folded x
            % unfold the searched orders
x = reshape(x, [s(o1),prod(s(o))]);

% to manage the slices manipulation 
s2 = size(x);
nd2 = length(s2);
inds2 = repmat({1},1,nd2);
for i=1:nd2
    inds2{i} = 1:s2(i);
end;

for i=1:lv % for successive extracted features

    if nargout > 1
        xu = reshape(x,N,prod(s2(2:end)));
        model.crbvx(i) = trace(xu*xu');
        model.crbvy(i) = trace(y*y');
        model.defx{i} = x;
    end;

    % search for max cov2
    cv = zeros(s2(end),1);
    
    for j=1:s2(end) % for all the slices

        % extract slice j
        inds2{end} = j;
        sl = squeeze(x(inds2{:}));
        
        % transpose the slice
        if length(size(sl))>2 
            slt = permute(sl,nd2-1:-1:1);
        else
            slt = sl';
        end
        % compute cov2
        m = n_mode_times(n_mode_times(slt,(y*y')),sl);
        cv(j) = sum(m(:));    
    end;
    
    if nargout>1
        model.covx{i} = squeeze(reshape(cv,[s(o) 1]));
    end
        % max covariance
    [~,is] = max(cv);
    nm(i) = is;
    
        % deflation of x and y
    switch deflat_type

             % deflation FM unweighted (full rank) 
        case 1                          
                % put the slice into u
            inds2{end} = is;
            u = squeeze(x(inds2{:}));
                % prepare u for matricial operation
            su = size(u);
            u = reshape(u,prod(su),1);
                
                % unfold x
            if length(s2)==2
                xu = x;
            else
                xu = reshape(x,prod(s2(1:end-1)),s2(end));
            end
                % orth zu
            m = (u'*xu)./(u'*u);
            xu = xu - u*m;
                % refold z
            x = reshape(xu,s2);
%                 % orth y
%             if length(u)==length(y)
%                 y = y - u*u'*y./(u'*u);
%             else
%                 u = reshape(u,N,prod(su(2:end)));
%                 y = y - u*pinv(u'*u)*u'*y;
%             end
            
            % deflation FM weighted (one rank)
        case 2                          
                % put the slice into u
            inds2{end} = is;
            u = squeeze(x(inds2{:}));
                % prepare u for matricial operation
            su = size(u);
            u = reshape(u,prod(su),1);
                
                % weight calculation
            xu = reshape(x,[prod(s2(1:end-1)),s(o)]);
            w = squeeze(n_mode_times(u',xu));
            
            if any(isnan(w)) | any(isinf(w))
                break;
            end;
% using PARAFAC
%             factors = parafac(w,1); 
%             
%             for k=length(factors):-1:1
%                 if length(factors{k}(:))==1
%                     factors(k)=[];
%                 end;
%             end;
%             
%             ww = factors{end}*factors{end}';
%             for k=length(factors)-1:-1:1
%                 ww = kron((factors{k}*factors{k}'),ww);
%             end

% using successive SVD
            sw = size(w);
            
            % if w is a vector
            if prod(sw) == max(sw)
                ww = w'*w;
            else
                % if w is a matrix
                if length(sw) ==2
                    [w1,~,w2] = svds(w,1);
                    ww = kron(w2*w2',w1*w1');
                else
                    % if w is n-way
                    ww = 1;
                    for k = 1:length(sw)-2
                        ssw = size(w);
                        m = reshape(w,ssw(1),prod(ssw(2:end)));
                        [v1,~,v2] = svds(m,1);
                        ww = kron(v1*v1',ww);
                        w = reshape(v2,ssw(2:end));
                    end
                    ww = kron(v2*v2',ww);
                end;
            end


            if length(s2)==2
                xu = x;
            else
                xu = reshape(x,prod(s2(1:end-1)),s2(end));
            end
                % orth zu
            m = (u'*xu);
            m = m * ww;
            xu = xu - u*m;
%            xu = xu - u * u' * xu * ww;
            
                % refold z
            x = reshape(xu,s2);
%                 % orth y
%             if length(u)==length(y)
%                 y = y - u*u'*y./(u'*u);
%             else
%                 u = reshape(u,N,prod(su(2:end)));
%                 y = y - u*pinv(u'*u)*u'*y;
%             end
            
    end;
    
    if nargout > 1
        % deflated x
        model.defx{i} = x;
    end;
    
    % stop the loop as soon nan or inf detected in X
    if any(isnan(x)) | any(isinf(x))
        break;
    end;
end;

% retrieving the index in the original tab
st='[';
for i=1:length(o)
    st= [st,sprintf('num(:,%d) ',i)];
end;
st = [st,'] = ind2sub(s(o),nm);'];
eval(st);

if nargout > 1
    % selection
    model.num = num;
    % evolution of the explained variance
    model.crbvx = 1-model.crbvx./model.crbvx(1);
    model.crbvy = 1-model.crbvy./model.crbvy(1);
end;




%% product A * B using the last mode of A and the first mode of B
function x = n_mode_times(a,b);
% n-mode product of 2 N-arrays
% x = n_mode_times(a,b);
%
% a is an N-array (I,...,N)
% b is an N-array (N,...,J)
%
% x is a * b (I,....,J)

dima = size(a);
dimb = size(b);

n = dima(end);

xa = reshape(a,prod(dima(1:end-1)),n);
xb = reshape(b,n,prod(dimb(2:end)));

x = reshape(xa*xb,[dima(1:end-1),dimb(2:end)]);



